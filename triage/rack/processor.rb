require 'rack'

require_relative '../triage/error'
require_relative '../triage/handler'
require_relative '../triage/sentry'
require_relative '../triage'

module Triage
  module Rack
    class Processor
      def call(env)
        event = env[:event]
        handler = Handler.new(event)

        puts "Running in dry mode" if Triage.dry_run?

        result = handler.process

        puts "Incoming webhook event: #{event.inspect}, messages: #{result.messages.inspect}"

        if result.errors.any?
          capture_errors(result.errors, event)
        end

        ::Rack::Response.new([JSON.dump(status: :ok, messages: result.messages)]).finish

      rescue Triage::ClientError => error
        error_response(error)
      rescue => error
        capture_error(error, event)
        error_response(error, status: 500)
      end

      private

      def error_response(error, status: 400)
        ::Rack::Response.new([JSON.dump(status: :error, error: error.class, message: error.message)], status).finish
      end

      def capture_errors(errors, event)
        errors.each { |error| capture_error(error, event) }
      end

      def capture_error(error, event)
        Raven.tags_context(object_kind: event.class.to_s)
        Raven.extra_context(event: event)
        Raven.capture_exception(error)

        puts "Exception with event: #{event.inspect}, error: #{error.class}: #{error.message}"
      end

      def dry_run?
        !ENV['DRY_RUN'].nil?
      end
    end
  end
end
