# frozen_string_literal: true

require_relative '../triage/triager'

module Triage
  class TypeLabel < Triager
    TYPE_LABELS = [
      'feature',
      'tooling'
    ].freeze

    def applicable?
      event.from_gitlab_org? &&
        subtype_label_added? &&
        !has_correct_type_label?
    end

    def process
      set_type_label
    end

    private

    def current_type_label
      (TYPE_LABELS & event.label_names).first
    end

    def added_subtype_label
      event.added_label_names.find do |label|
        TYPE_LABELS.find { |type_label| label.start_with?("#{type_label}::") }
      end
    end

    def subtype_label_added?
      !!added_subtype_label
    end

    def new_type_label
      return unless subtype_label_added?

      added_subtype_label.sub(/::.+\z/, '')
    end

    def has_correct_type_label?
      current_type_label == new_type_label
    end

    def type_labels_to_remove
      TYPE_LABELS.reject { |label| label == new_type_label }
    end

    def set_type_label
      add_comment <<~MARKDOWN.chomp
        /label ~"#{new_type_label}"
        /unlabel #{type_labels_to_remove.map { |l| %Q(~"#{l}") }.join(' ')}
      MARKDOWN
    end
  end
end
