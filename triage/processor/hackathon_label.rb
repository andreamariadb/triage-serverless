# frozen_string_literal: true

require 'time'
require_relative '../triage/triager'

module Triage
  class HackathonLabel < Triager
    HACKATHON_START_DATE = Time.parse('2021-06-01T12:00:00Z')
    HACKATHON_END_DATE = Time.parse('2021-06-04T12:00:00Z')
    HACKATHON_TRACKING_ISSUE = 'https://gitlab.com/gitlab-com/marketing/community-relations/contributor-program/hackathon/-/issues/52'

    def applicable?
      event.merge_request? &&
        gitlab_org_or_gitlab_com? &&
        created_during_hackathon? &&
        community_contribution_label_added?
    end

    def process
      label_hackathon
    end

    private

    def label_hackathon
      add_comment <<~MARKDOWN.chomp
        This MR will be considered [part of](#{HACKATHON_TRACKING_ISSUE}) the quarterly [GitLab Hackathon](https://about.gitlab.com/community/hackathon/) for a chance to win a [prize](https://about.gitlab.com/community/hackathon/#prize). Please link the relevant issue in the description that this merge request is related to or closing.

        Thank you for your contribution! :heart:

        /label ~Hackathon
      MARKDOWN
    end

    def gitlab_org_or_gitlab_com?
      event.from_gitlab_org? || event.from_gitlab_com?
    end

    def community_contribution_label_added?
      event.added_label_names.include?('Community contribution')
    end

    def created_during_hackathon?
      event.created_at >= HACKATHON_START_DATE &&
        event.created_at < HACKATHON_END_DATE
    end
  end
end
